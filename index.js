/**
* get first image from google images
*
* @param {string} description
* @return {object}
*/
var request = require('request'),
    http = require('http');

var googleImageApiUrl = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=";

var defaultOptions = {
    position: 0,
    minWidth: 250
};

module.exports = {
    getImage: function(description, callback, options) {
        if (typeof options === 'undefined') options = defaultOptions;
        
        description = description.replace(/[¼½¾]/g, '');
        var url = googleImageApiUrl + encodeURIComponent(description);
           
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                
                var results = JSON.parse(body)['responseData'].results;
                
                if (!results) {
                    callback (null);
                    return;
                }
                
                returnFirstExistingResource(results, options.position, options.minWidth, callback);
            }
       });
    }
}


var returnFirstExistingResource = function(list, index, minWidth, success) {
    if(index >= list.length) {
        success(null);
        return;
    }
    
    var url = list[index].unescapedUrl;
    var options = {method: 'HEAD', url: url};
    
    var req = http.request(options, function(res) {
        var exists = (Number(res.statusCode) === 200);
        if (exists && list[index].width >= minWidth) {
            
            var jsonContent = list[index];
            imageDetails = {
                    url: jsonContent.unescapedUrl,
                    width: jsonContent.width,
                    height: jsonContent.height,
                    position: index
            };
            
            success(imageDetails);
        } else {
            returnFirstExistingResource(list, index++, minWidth, success);
        }
      });
    
    req.end();
}
