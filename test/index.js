var should = require('chai').should(),
    getpic = require('../index'),
    getImage = getpic.getImage;
    getMockImage = getpic.getMockImage;

describe('#getImage', function() {
 it('returns Tidcombe Lane', function(done) {
   setTimeout( function() {
        getImage('Tidcombe Lane',function(callback){
        	try {
        		callback.url.should.equal("http://li.zoocdn.com/5a64ec78e168e255c4d2c02ca293f71ac6bdf1d3_645_430.jpg");
        		done();
        	} catch (e) {
        		done(e)
        	}
        });
   }, 300);
 });      
});

describe('#getImageImproved', function() {
	it('actually returns a bridge', function(done) {
		setTimeout( function() {
			getImage('Tidcombe Lane',function(callback){
				try {
					callback.url.should.equal("http://static.panoramio.com/photos/large/3874908.jpg");
					done();
				} catch (e) {
					done(e);
				}
			});
		}, 1);
	});      
});